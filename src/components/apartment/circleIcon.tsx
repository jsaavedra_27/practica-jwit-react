import { Children } from "react"
import styled from "styled-components"

const Container = styled.div`    
    border-radius: 50%;
    height: 50px;
    width: 50px;
    background-color: rgba(255, 255, 255, 0.3);
    display: flex;
    justify-content: center;
    align-items:center;
`
const Icon = styled.img`
    height: 20px;
    width: 20px;
    display: block;
`


interface params  {
  icon: string,
  style?: React.CSSProperties
  children?: React.ReactNode
}

const App = (params:params) => {
  return (
    <Container style={params.style}>
        <Icon src={params.icon}/>
        {params.children}
    </Container>
  )
}

export default App