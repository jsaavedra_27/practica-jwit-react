import styled from "styled-components"
import image from '../../assets/apartment.jpg'
import InfoIcon, {params as paramsComponentInfo} from './infoIcon'
import arrow from '../../assets/arrow.png'
import starLight from '../../assets/star-light.png'
import bed from '../../assets/bed.png'
import bath from '../../assets/shower.png'
import parking from '../../assets/parking.png'
import gym from '../../assets/weight.png'

const Container = styled.div<{image:string}>`
    font-family: Arial, Helvetica, sans-serif;
    border-radius: 2.5rem;
    background-image: url(${props => props.image});
    background-position: -31px -58px;
    color: #FBFDFE;
    width: 366px;
    height: 600px;
    margin: 0 auto;
    padding: 3rem 0 0 0 ;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-between;
    font-weight:bold;
    
`
const Body = styled.div`
  border-radius:  2.5rem;  
  background-color: rgba(111, 167, 187 , 0.5); 
  backdrop-filter: blur(5px) contrast(57%);
  flex: 0 0 70%;
  align-self: auto;
  width: 100%;
  padding: 1.8rem 1.2rem;  
  box-sizing: border-box;
  position: relative;
`
const Rent = styled.div`
  display: flex;
  gap: 6px; 
  align-items: center;
`
const Shape = styled.div`
  position: absolute;
  top: 15px;
  right: 15px;
  border-right: 3px solid #fff;
  border-radius: 51% 49% 50% 50% / 51% 51% 49% 49%;
  height: 30px;
  width: 20px;
  transform: rotate(-44deg);
`

const Title = styled.h2`
  font-size: 2.5rem;
  margin: 8px 0 0 0;  
  line-height: 1;
`
const Text = styled.p`
  font-size: .8rem;
`
const House = styled.div`
  display: flex; 
  justify-content: space-between;
`

const Footer = styled.div`
  margin-top: 2rem;
  display: flex;
  justify-content: space-between;
  align-items: center;
`
const Price = styled.div`
  display: flex;
  flex-direction: column;  
  flex: 0 0 30%;
`

const Button = styled.button`
  border-radius: 2rem;
  border: 0;
  flex: 0 0 58%;
  background-color: #fff;
  color: #565656;
  font-weight: bold;
  padding: 1rem;
`


interface params {

}

const App = (params: params) => {
  const housePart:paramsComponentInfo[] = [
    {description: 'Bedrooms', icon: bed, textBadge: '3', smallDescription: true},
    {description: 'Bathrooms', icon: bath, textBadge: '2', smallDescription: true},
    {description: 'Parking', icon: parking, smallDescription: true},
    {description: 'Gym', icon: gym, smallDescription: true},
  ]

  return (
    <Container image={ image}>
        <InfoIcon icon={arrow} description={"Go back"} />
        <Body>
          <Rent>
            <img src={starLight} style={{height: "10px", width: "10px"}}/>
            <span style={{fontSize: ".8rem"}}>Affitto</span>
          </Rent>        
          <Shape/>
          <Title>Opulence Apartment</Title>
          <Text>Celina, Delaware</Text>
          <Text>
            You Will Have Everything Neorby: Supermorket,
            Banses, Stations, Cinemas, Theaters, The Carmen 
            Neighborhood, Etc.
          </Text>
          <House>
            {housePart.map((v, i) => <InfoIcon key={i} {...v}/>)}
          </House>
          <Footer>
            <Price>
              <Text style={{margin: '0', fontSize: '1.2rem'}}>$5,680</Text>
              <Text style={{margin: '0'}}>Month</Text>
            </Price>
            <Button>Book Now</Button>
          </Footer>
        </Body>        
    </Container>
  )
}

export default App