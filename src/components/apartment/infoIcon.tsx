import React from 'react'
import styled from 'styled-components';
import CircleIcon from './circleIcon'

const Container = styled.div`
    display: flex;
    flex-direction: column;
    gap: 10px;
    align-items: center;
`
const Description = styled.p<{small:boolean}>`
    font-weight: bold;
    color: #fff;
    margin: 0;
    font-size: ${props=> props.small ? ".8rem": "inherit"};
`
const Badge = styled.div`
    border-radius: 50%;
    background-color: #fff;
    height: 1rem;
    width: 1rem;
    color: #9E9E9E;
    font-size: .7rem;
    font-weight: bold;
    text-align: center;
    line-height: 1rem;
    position: absolute;
    right: 0;
    top: 0;
`

export interface params {
    description: string;    
    icon: string;
    textBadge?: string;
    style?: React.CSSProperties;
    smallDescription: boolean ;
}

const App = (params: params) => {
     
  return (
    <Container>
        <CircleIcon icon={params.icon} style={{position:'relative'}}>        
            {params.textBadge && <Badge>{params.textBadge}</Badge>}
        </CircleIcon>        
        <Description small={params.smallDescription}>{params.description}</Description>
    </Container>
  )
}

App.defaultProps = {
    smallDescription: false
}

export default App